'use strict'

const express = require('express')
const app = express()

const port = 8000

app.get('/movies', (req, res) => {
    const results = {
        results: [{
            title: 'fake title 1',
            image_url: 'fake image url',
            overview: 'fake overview'
        }, {
            title: 'fake title 1',
            image_url: 'fake image url',
            overview: 'fake overview'
        }]
    }

    res.json(results)
})

app.listen(8000, () =>
    console.log(`started at ${port}`))
